# Changelog

## [1.3] - 2018-10-25
### Changed
- Update React plugin to latest changes

Agora 7.x-1.x
==============
# Rename module to agora
# Add functionality to create multiple API feeds
# Add functionality to create multiple drupal pages to render the API
